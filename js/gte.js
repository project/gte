
  function saveAs(uri, filename) {
      var link = document.createElement('a');
      if (typeof link.download === 'string') {
          link.href = uri;
          link.download = filename;
          //Firefox requires the link to be in the body
          document.body.appendChild(link);
          //simulate click
          link.click();
          //remove the link when done
          document.body.removeChild(link);
      } else {
          window.open(uri);
      }
  }

(function($){
  $(document).ready(function(){
        $("body").on("click",".gte",function()  {
            var element_id = $(this).attr("data-element");
            var host = $(this).attr("data-host");
            if ( typeof element_id !== 'undefined'  ) {
              const el = document.getElementById( element_id );
              console.log(el);
              html2canvas(el , {
                    scrollX: 0,
                    scrollY: -window.scrollY
                  } ).then(function(canvas) {
                    saveAs(canvas.toDataURL(), host.replace(/\./g, '-')  + '-' + element_id + '.png');
              });
            }
        });
  });
})(jQuery);
