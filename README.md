*Grab that Element (GTE)*
By: afagioli@drupal.org


Desription:
==========
GTE provides a screenshot fearture for Drupal 8 DOM elements.



Use cases:
==========
 * create a screenshot to be shared with more people



Install:
========

A). composer require drupal/gte
B). Download the library from https://html2canvas.hertzen.com/ so that there's a https://YOURSITE/libraries/html2canvas/html2canvas.min.js file
C). drush en gte



Use:
========

. Create in your MYTHEME.theme  preprocess function a twig variable
. use that varible inside your templates




