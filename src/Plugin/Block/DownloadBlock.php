<?php

//https://www.drupal.org/docs/8/creating-custom-modules/creating-custom-blocks/create-a-custom-block

namespace Drupal\gte\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'GetPicture' Block.
 *
 * @Block(
 *   id = "getpicture_block",
 *   admin_label = @Translation("Get picture block"),
 *   category = @Translation("Get picture"),
 * )
 */
class DownloadBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => gte_add_download_click($settings = Array()),
      '#theme' => 'gte', // not working
      '#attached' => [
        'library' => [
          'gte/gte',
        ],
      ],
    ];
  }
}
